# [Pusheen Loves Graphs](https://app.hackthebox.eu/challenges/Pusheen-Loves-Graphs)

#### Challenge Description

Pusheen just loves graphs, Graphs and IDA. Did you know cats are weirdly controlling about their reverse engineering tools? Pusheen just won't use anything except IDA.


###### The Flag
> `HTB{fUn_w17h_CFGz}`

---
## Finding the Flag


#### Initial Investigation
After unzipping the file, I ran `file` to see what I was dealing with; it came back as an `ELF` file. This is basically a binary file for Linux.

![ELF](./images/elf.png)

Out of curiosity, I ran the file in a sandboxed environment, and was greeted with an ASCII cat.

![The Cat](./images/the_cat.png)

Cool, not the flag I was looking for. I ran `strings` on a whim, but found nothing. So, I moved on to opening it up in **IDA**.

#### IDA
When I opened up the binary in IDA, there were a lot of alert messages, mostly benign, except for one. This is one that I hadn't seen before. This stated that the number of nodes in the graph exceeded the limit that could be displayed at any one time.

![The Message](./images/ida_warning.png)

After a bit of searching around, I found that I could increase this limit quite easily. So I changed it from *1000* to *10000*. I quit out of **IDA** and reopened the binary. Initially the graph was quite zoomed in, so I set it to fit the window. An lo and behold, the flag drawn in the graph... Cool!

![The Flag](./images/graph_flag.png)
