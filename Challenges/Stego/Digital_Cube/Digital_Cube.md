# [Digital Cube](https://app.hackthebox.eu/challenges/Digital-Cube)

#### Challenge Description

...Strathmore leaned forward and rotated his monitor toward Susan. The screen was black except for a small, white text box blinking in the middle. TIME ELAPSED: 50:50"
TRANSLTR, the single best crypto-machine, could not crack this code. Maybe you can...


###### Flag(s)
> `HTB{QR_!snt_d34d}`

---
## Finding the Flag

### Initial Investigation
After downloading and extracting the file, I ran `file digitalcube.txt` to see what I was dealing. The result was unsurprisingly an ASCII file.

![ASCII?](./images/file.png)

Next up was to take a look at the file's contents. When looking at the contents, I thought I could make out some patterns with text; as if the ones and zeros were lining up to form some sort of picture.

![Something in th fog?](./images/something.png)

No matter how much I tried play around with the width, I could get anything to line up correctly. Then I had an idea based on the name of the challenge: *What if this makes a cube?*. So I counted the number of characters (not by hand) using the `wc` command `wc --chars digitalcube.txt`. The result of which was 2502 characters. The square root of 2502 is about **50**. So I set the width of the text to that, and could make out some shapes such as squares.

![Let's count them...](./images/num_chars.png)

![sqrt](./images/sqrt.png)

![Squares?](./images/50x50.png)

I couldn't quite see what it was I needed to see from this.


### The Code
I took the contents of the file and copied them into a [cipher identifier](https://www.dcode.fr/cipher-identifier). It was identified as one of many things.

![What is it?](./images/identify.png)

I tried the **Binary Code** first as it looked the most plausible, however that just gave me garbage characters out. The one which stood out to me was the **Image in Binary 0 1** as this was what I had tried to do before.

I copied the text into the text field and set the width to 50, and what do you know, a QR code!

![QR!](./images/qr.png)

I downloaded the image and opened it up in [CyberChef](https://gchq.github.io/CyberChef/), and was greeted with the flag.

![The Flag](./images/the_flag.png)

### Final Attempt?
When I look back at my attempt to make a picture out of the text, I can now see the resemblance to a QR code, but may not have been able to scan it as it would have been too pixelated.

Out of curiosity, I did try to make the font size as small as I possibly could and scan it with a QR reader on my phone, but I just couldn't get it to work.

![Scanning with my phone!](./images/attempt.png)
