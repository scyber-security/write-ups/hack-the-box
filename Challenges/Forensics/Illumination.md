# [Illumination](https://app.hackthebox.eu/challenges/Illumination)

#### Challenge Description
A Junior Developer just switched to a new source control platform. Can you find the secret token?

###### The Flag
> `HTB{v3rsi0n\_c0ntr0l\_am\_I\_right?}`

---
## Finding the flag
After unzipping the archive attached to this challenge, I'm left with a directory which contains a `.git` directory.

Running `git status`, I get the following output:
```sh
On branch master  
Changes not staged for commit:  
 (use "git add <file>..." to update what will be committed)  
 (use "git restore <file>..." to discard changes in working directory)  
       modified:   bot.js  
       modified:   config.json  
  
no changes added to commit (use "git add" and/or "git commit -a")
```

The changes don't appear to be anything of interest; just a load of `^M` added to the ends of every line, which I'm assuming is just an artefact of the zipping process.


##### config.json
I first looked at **config.json**, and there was a *username* declared with the value of `UmVkIEhlcnJpbmcsIHJlYWQgdGhlIEpTIGNhcmVmdWxseQ==`; a **Base64** encoded string. But when decoding it, I get the following message:
> `Red Herring, read the JS carefully`

Not what I'm looking for, but a useful hint nonetheless.

##### Looking at the Javascript
Looking at the javascript file, there's a line commented out which writes some data to the config file:

![The Comment](./images/Illumination-Comment.png)

So at some point, a string would have been written out to the config file for testing/debug purposes.

##### Git Log
I ran the command `git log` to see the commit history:
![Git Log](./images/Illumination-Git_Log.png)

Lo and behold, there's a comment which states a *unique token* was removed. I checked out the previous version (`git checkout ddc606f8fa05c363ea4de20f31834e97dd527381`) and took a look at the **config.json** file.

Found it! The token was written out to the config file and committed to the repo:

![The Token](./images/Illumination-The_Token.png)

Another **Base64** encoded string, that when decoded gave me the flag of:
```
echo SFRCe3YzcnNpMG5fYzBudHIwbF9hbV9JX3JpZ2h0P30= | base64 -d  
# HTB{v3rsi0n\_c0ntr0l\_am\_I\_right?}
```
