# [USB Ripper](https://app.hackthebox.eu/challenges/USB-Ripper)

#### Challenge Description

There is a sysadmin, who has been dumping all the USB events on his Linux host all the year... Recently, some bad guys managed to steal some data from his machine when they broke into the office. Can you help him to put a tail on the intruders? Note: once you find it, "crack"

###### The Flag
> `HTB{mychemicalromance}`

---
## Finding the Flag

#### BASH
Taking a look at **auth.json**, it looks like it contains a list of *manufacturer* IDs which potentially match up with manufacturer IDs.

My initial approach was to use some *bash-fu* to see if I could find any values from the **syslog** which aren't in **auth.json**.

```bash
grep Manufact syslog | awk '{print $(NF)}' | xargs -I {} grep {} -Fxq auth.json || echo {}
```

After many failed iterations, I resorted to creating a Python script to do the job.


#### Python
My first version of the script looked like this:

```python
#!/usr/bin/env python3

with open("auth.json", mode='r') as j_file:
    j_data = j_file.read()

with open("syslog", mode='r') as s_file:
    syslog = s_file.readlines()

for line in syslog:
    if "Manufacturer" not in line:
        continue

    arr = line.split(":")[-1].strip()

    if arr not in j_data:
        print(arr)
```

However, no results were returned. I swapped out the *Manufacturer* string I was initially looking for with *SerialNumber:*. I added `:` to the end as it was returning many lines like this: `Mfr=1, Product=2, SerialNumber=3`.

Once all of this was ironed out, the script eventually returned a single result:

![Found It](./images/USB_Ripper-Found_It.png)

I copied the string and pasted it into [crackstation](https://crackstation.net/) where it was immediately identified as an **MD5** hash.

![Crackstation](./images/USB_Ripper-Crackstation.png)
