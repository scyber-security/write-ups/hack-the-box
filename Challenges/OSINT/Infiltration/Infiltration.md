# [Infiltration](https://app.hackthebox.eu/challenges/Infiltration)

#### Challenge Description

Can you find something to help you break into the company 'Evil Corp LLC'. Recon social media sites to see if you can find any useful information.


###### The Flag
> `HTB{Y0ur_Enum3rat10n_1s_Str0ng_Y0ung_0ne}`

---
## Finding the Flag

> Reference to Mr. Robot.

#### Their Website?
I started off by just searching `Evil Corp LLC` and came up with a number of results. I took a look at the [webiste](https://www.evil-corp.org/), but looking round that really only led me to an [Instagram page](https://www.instagram.com/scottie_doesnt_know_/?hl=en) which I couldn't access.


#### LinkedIn

Also in the search results was a LinkedIn [company page](https://www.linkedin.com/company/evil-corp-llc/). There is what looks to be the flag, but is apparently invalid... Nuts.

![A Flag!](./images/linkedin-company.png)

The contents look to be Base64 encoded, which when decoded give a nice message of encouragement: `You can do this, keep going!!!`

Scrolling down through the search results I see another result for LinkedIn, this time someone claiming to work for the company, interesting.

The profile belongs to a [Brian Delany](https://www.linkedin.com/in/brian-delany-2ab807195/), and in their about me section, another Base64 encoded string; quite a long one this time.

![Mr. Delany](./images/linkedin-delany.png)
![Chef](./images/cyberchef-delany.png)

#### Instagram
I initially discounted Instagram as I don't have an account with them, nor wanted one. But I was reminded that maybe some *Google Dorking* might be the key here. Using the search term `Evil Corp LLC inrul:instagram.com` I was greeted with a few results, the first being for an account which I think was a legitimate user.

But interestingly, the next result was that of a [picture](https://www.instagram.com/p/BvbnFhTj9YS/) with an Evil Corp name badge on it.

![The Badge](./images/instagram.jpeg)


Taking a good look at the picture, there's not much on there that's immediately obvious. But on closer inspection of the name badge, right at the bottom is the flag we have been looking for.
